
AddTargetLibrary(
    LIBRARY_NAME
        MRichTextEdit
    SOURCES
        mrichtextedit.cpp
        mtextedit.cpp
    HEADERS
        mrichtextedit.h
        mtextedit.h
    DESIGNER_FORMS
        mrichtextedit.ui
    LIBRARIES
        Qt${QT_MAJOR_VERSION}::Core
        Qt${QT_MAJOR_VERSION}::Gui
        Qt${QT_MAJOR_VERSION}::Widgets
)
set_property(TARGET MRichTextEdit PROPERTY CXX_STANDARD 17)
